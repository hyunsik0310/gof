package com.siku.gof.singleton;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class MultiThreadSingletonSettings4 {

  private static class SettingHolder {

    private static final MultiThreadSingletonSettings4 INSTANCE =
        new MultiThreadSingletonSettings4();

  }

  public static MultiThreadSingletonSettings4 getInstance() {
    return SettingHolder.INSTANCE;
  }


}
