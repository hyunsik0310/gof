package com.siku.gof.singleton;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class MultiThreadSingletonSettings3 {

  private static volatile MultiThreadSingletonSettings3 instance;

  // double checked locking
  public static MultiThreadSingletonSettings3 getInstance() {

    if (instance == null) {
      synchronized (MultiThreadSingletonSettings3.class) {
        if (instance == null) {
          instance = new MultiThreadSingletonSettings3();
        }
      }
    }
    return instance;
  }

}
