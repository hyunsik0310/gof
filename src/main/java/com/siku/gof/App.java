package com.siku.gof;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import com.siku.gof.singleton.MultiThreadSingletonSettings4;

public class App {

  public static void main(String[] args) throws NoSuchMethodException, Exception {

    // // 1. Singleton Pattern
    // NomalSettings nomalSettings1 = new NomalSettings();
    // System.out.println("nomalSettings1 : " + nomalSettings1);
    //
    // NomalSettings nomalSettings2 = new NomalSettings();
    // System.out.println("nomalSettings2 : " + nomalSettings2);
    //
    // System.out.println(nomalSettings1 == nomalSettings2);
    //
    // SingletonSettings setting1 = SingletonSettings.getInstance();
    // System.out.println("setting1 : " + setting1);
    //
    // SingletonSettings setting2 = SingletonSettings.getInstance();
    // System.out.println("setting2 : " + setting2);
    //
    // System.out.println(setting1 == setting2);
    //
    //
    // SingletonSettings settings = SingletonSettings.getInstance();
    //
    // // 리플렉션
    // Constructor<SingletonSettings> constructor =
    // SingletonSettings.class.getDeclaredConstructor();
    //
    // // private생성자에 접근이 가능하게 됨
    // constructor.setAccessible(true);
    //
    // SingletonSettings settingsNotEqual = constructor.newInstance();
    //
    // System.out.print("settings == settingsNotEqual : ");
    // System.out.println(settings == settingsNotEqual);
    //
    //
    //

    MultiThreadSingletonSettings4 settings = MultiThreadSingletonSettings4.getInstance();

    MultiThreadSingletonSettings4 settings1 = null;
    // file생성
    try (ObjectOutput out = new ObjectOutputStream(new FileOutputStream("setting.obj"))) {
      out.writeObject(settings);
    }

    try (ObjectInput in = new ObjectInputStream(new FileInputStream("setting.obj"))) {
      settings1 = (MultiThreadSingletonSettings4) in.readObject();
    }

    System.out.println(settings == settings1);

  }
}
