package com.siku.gof.singleton;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class MultiThreadSingletonSettings2 {

  private static MultiThreadSingletonSettings2 instance;

  // synchronized를 추가하는 것으로 여러 쓰레드에서 동시에 getInstance()에 접근하지 못하게함
  // 대신 성능면에서 좋지 않은 단점이 있음
  public static synchronized MultiThreadSingletonSettings2 getInstance() {

    if (instance == null) {
      instance = new MultiThreadSingletonSettings2();
    }

    return instance;
  }

}
