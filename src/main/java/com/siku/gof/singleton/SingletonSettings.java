package com.siku.gof.singleton;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class SingletonSettings {

  private static SingletonSettings instance;

  // @NoArgsConstructor가 있기 때문에 안써도 됨
  // private Settings() {}

  public static SingletonSettings getInstance() {

    // 필드instance가 없을 경우 new 한다.
    // ※주의 쓰레드세이프 하지 않음
    // null체크 후 new 하기전 다른 쓰레드에서 null체크에 접근할 경우 서로 다른 인스턴스를 생성할 경우가 있음
    if (instance == null) {
      instance = new SingletonSettings();
    }

    return instance;
  }

}
