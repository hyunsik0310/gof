package com.siku.gof.singleton;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class MultiThreadSingletonSettings1 {

  // 이른 초기화(eager initialization)
  // 안쓰는 객체를 미리 만들게 됨(사용하지 않아도 메모리를 차지하게 됨)
  private static final MultiThreadSingletonSettings1 INSTANCE = new MultiThreadSingletonSettings1();

  public static MultiThreadSingletonSettings1 getInstance() {
    return INSTANCE;
  }

}
